#usr/bin/env bash

DROPBEAR_SSH_PORT=22022
WARBAND_SERVER_PORT=7240
WARBAND_STEAM_PORT=7241
IMAGE='duckhp/warbandviking:latest'
#DOCKER_CONTAINER_NAME="mb-warband-viking-conquest-server"

docker run -ti \
	-p $DROPBEAR_SSH_PORT:2222 \
	-p $WARBAND_SERVER_PORT:7240 \
	-p $WARBAND_STEAM_PORT:7241 \
	$IMAGE

#/usr/bin/sh
# warband server docker ssh login script


if [ $(whoami) == "root" ]
then
	echo "SSH as 'root' user not accepted!"
	exit 126
fi

#first time logging in?
#making sure to set a different password than the default
if [ $(whoami) == "warbandserver" ] && [ -f /home/warbandserver/.fresh ]
then
	if [ $(cat /home/warbandserver/.fresh) -lt 1 ]
	then
		echo "Welcome to your fresh M&B:Warband - Viking Conquest Sedicated Server!"
		echo 1 > /home/warbandserver/.fresh
	fi

	if  [ $(cat /home/warbandserver/.fresh) -lt 2 ]
	then
		echo "Set a new password now!" 
		passwd
		while [ $? -ne 0 ]
		do
			echo "You need to set a new login password NOW!"
			passwd
		done
		if [ $? -eq 0 ]
		then
			echo 2 > /home/warbandserver/.fresh
		else
			exit 126
		fi
	fi
fi

#export PS1="$(whoami)@$(hostname)$ "
tmux a -t warbandsrv
